"use strict";
// async & await
// clear style of using promise :)

// 1. async
function fetchUser() {
  return new Promise((resolve, reject) => {
    // do network request in 10 secs...
    //resolve("ellie");
  });
}

async function asyncFetchUser() {
  // do network request in 10 secs...
  return "ellie";
}

let user = fetchUser();
user.then(console.log);

user = asyncFetchUser();
user.then(console.log);

// 2. await
function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function getApple() {
  await delay(2000);
  return "🍎";
}
// function getApple() {
//   return delay(3000).then(() => "🍎");
// }

async function getBanana() {
  await delay(1000);
  return "🍌";
}
// function getBanana() {
//   return delay(3000).then(() => "🍌");
// }

function pickFruits() {
  return getApple().then((apple) => {
    return getBanana().then((banana) => `${apple} + ${banana}`);
  });
}
pickFruits().then(console.log);

async function asyncPickFruits() {
  const apple = await getApple();
  const banana = await getBanana();
  return `async: ${apple} + ${banana}`;
}
asyncPickFruits().then(console.log);

async function asyncPickFruitsSoon() {
  const applePromise = getApple();
  const bananaPromise = getBanana();
  const apple = await applePromise;
  const banana = await bananaPromise;
  return `async soon: ${apple} + ${banana}`;
}
asyncPickFruitsSoon().then(console.log);

// 3. useful Promise APIs
function pickAllFruits() {
  return Promise.all([getApple(), getBanana()]).then((fruits) =>
    fruits.join(" + ")
  );
}
pickAllFruits().then((fruits) => console.log(`pick all: ${fruits}`));

function pickOnlyOne() {
  return Promise.race([getApple(), getBanana()]);
}
pickOnlyOne().then(console.log);
