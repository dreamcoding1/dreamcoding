"use strict";

// Promise is a JavaScript object for asynchronous operation.
// state: pending -> fulfilled or rejeted
// Producer vs Consumer

// 1. Producer
// when new Promise is created, the executor runs automatically.
const promise = new Promise((resolve, reject) => {
  // doing some heavy work (network, read files);
  console.log("doing something");
  setTimeout(() => {
    resolve("ellie");
  }, 2000);
});

const errorPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject(new Error("no network"));
  }, 3000);
});

// 2. Consumers: then, catch, finally
promise.then((value) => {
  console.log(value);
});

errorPromise
  .then((value) => {
    console.log(value);
  })
  .catch((error) => {
    console.log(error);
  })
  .finally(() => {
    console.log("finally");
  });

// 3. Promise chaining

const fetchNumber = new Promise((resolve, reject) => {
  setTimeout(() => resolve(1), 3000);
});

fetchNumber
  .then((num) => num * 2)
  .then((num) => num * 3)
  .then((num) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(num - 1), 3000);
    });
  })
  .then((num) => console.log(num));

// 4. Error Handling
const getHen = () =>
  new Promise((resolve, reject) => {
    setTimeout(() => resolve("🐓"), 5000);
  });
const getEgg = (hen) =>
  new Promise((resolve, reject) => {
    //setTimeout(() => resolve(`${hen} => 🥚`), 5000);
    setTimeout(() => reject(new Error(`error! ${hen} => 🥚`)), 5000);
  });
const cook = (egg) =>
  new Promise((resolve, reject) => {
    setTimeout(() => resolve(`${egg} => 🍳`), 5000);
  });

getHen()
  .then((hen) => getEgg(hen)) // .then(getEgg)
  .catch((error) => {
    return "🍞";
  })
  .then((egg) => cook(egg)) // .then(cook)
  .then((meal) => console.log(meal)) // .then(console.log)
  .catch((err) => console.log(err)); // .catch(console.log)
