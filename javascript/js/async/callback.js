"use strict";

// JavaScript is synchronous.
// Excute the code bolck in order after hoisting.
// hoisting: var, function declaration
console.log("1");
setTimeout(() => console.log("2"), 10000);
console.log("3");

// Synchronous callback
function printImmediately(print) {
  console.log("4");
  print();
}
printImmediately(() => console.log("hello"));

// Asynchronous callback
function printWithDelay(print, timeout) {
  console.log("5");
  setTimeout(print, timeout);
}
printWithDelay(() => console.log("async callback"), 5000);

// Callback Hell example
class UserStorage {
  loginUser(id, password, onSuccess, onError) {
    setTimeout(() => {
      if (
        (id === "ellie" && password === "dream") ||
        (id === "color" && password === "academy")
      ) {
        onSuccess(id);
      } else {
        onError(new Error("not found"));
      }
    }, 2000);
  }

  getRoles(user, onSuccess, onError) {
    setTimeout(() => {
      if (user === "ellie") {
        onSuccess({ name: "ellie", role: "admin" });
      } else {
        onError(new Error("no access"));
      }
    }, 1000);
  }
}

const userStrage = new UserStorage();
const id = prompt("enter your id");
const password = prompt("enter your password");
userStrage.loginUser(
  id,
  password,
  (user) => {
    userStrage.getRoles(
      user,
      (userWithRole) => {
        alert(
          `Hello ${userWithRole.name}, you have a ${userWithRole.role} role`
        );
      },
      (error) => {
        console.log(error);
      }
    );
  },
  (error) => {
    console.log(error);
  }
);
