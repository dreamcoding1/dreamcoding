"use strict";
// JSON
//JavaScript Object Notation

// 1. Object to JSON
// stringify(obj)
let json = JSON.stringify(true);
console.log(json);

json = JSON.stringify(["apple", "banana"]);
console.log(json);

const rabbit = {
  name: "tori",
  color: "white",
  size: null,
  birthDate: new Date(),
  // 객체의 메소드를 화살표 함수로 선언할 경우 this는 전역객체를 가르키기 때문에 window가 출력된대요!
  //   jump: () => {
  //     console.log(`${this.name} can jump!`);
  //   }
  jump: function () {
    console.log(`${this.name} can jump!`);
  }
  // or
  //   jump() {
  //     console.log(`${this.name} can jump!`);
  //   }
};

json = JSON.stringify(rabbit);
console.log(json);

json = JSON.stringify(rabbit, ["name", "color"]);
console.log(json);

json = JSON.stringify(rabbit, (key, value) => {
  console.log(`key: ${key}, value: ${value}`);
  return key === "name" ? "ellie" : value;
});
console.log(json);

// 2. JSON to Object
// parse(json)
console.clear();
json = JSON.stringify(rabbit);
const obj = JSON.parse(json);
console.log(obj);
rabbit.jump();
// obj.jump(); // Uncaught TypeError: obj.jump is not a function

console.log(rabbit.birthDate.getDate());
// console.log(obj.birthDate.getDate()); // Uncaught TypeError: obj.birthDate.getDate is not a function

const reviverObj = JSON.parse(json, (key, value) => {
  console.log(`key: ${key}, value: ${value}`);
  return key === "birthDate" ? new Date(value) : value;
});
console.log(reviverObj);
console.log(reviverObj.birthDate.getDate());
