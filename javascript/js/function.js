"use stict";
// Functions
//  - fundamental building block in the program
//  - subprogram can be used multiple times
//  - performs a task or calculates a value

// 1. Function declaration
// function name(param1, param2) { body... return; }
// one function === one thing
// naming: doSomething, command, verb
// e.g. createCardAndPoint -> createCate, createPoint
// function is object is JS
function printHello() {
  console.log("Hello");
}
printHello();

function log(message) {
  console.log(message);
}
log("hello@");
log(1234);

// 2. parameters
// primitive parameters: passed by value
// object parameters: passed by reference
function changeName(obj) {
  obj.name = "coder";
}
const ellie = { name: "ellie" };
changeName(ellie);
console.log(ellie);

// 3. Default parameters (added in ES6)
// function showMessage(message, from) {
function showMessage(message, from = "unknown") {
  // if (from === undefined) {
  //   from = "unkown";
  // }
  console.log(`${message} by ${from}`);
}
//showMessage("Hi!");
showMessage("Hi!", "Gibbs");

// 4. Rest parameters (added in ES6)
function printAll(...args) {
  for (let i = 0; i < args.length; i++) {
    console.log(args[i]);
  }

  for (const arg of args) {
    console.log(arg);
  }

  args.forEach((arg) => console.log(arg));
}
printAll("dream", "coding", "ellie");

// 5. Local scope, 안에서는 밖이 보이고 밖에선 안을 볼 수 없다.
let globalMessage = "golbal"; // global variable
function printMessage() {
  let message = "hello"; // local variable
  console.log(message);
  console.log(globalMessage);
  function printAnother() {
    console.log(message);
    let childMessage = "hello";
  }
  // console.log(childMessage); // scope error
}
printMessage();
// console.log(message); // scope error

// 6. Return a value, return 이 없는 함수는 'return undefined;' 와 같다.
function sum(a, b) {
  return a + b;
}
const result = sum(1, 2); // 3
console.log(`sum: ${sum(1, 2)}`);

// 7. Early return, early exit
// bad
function upgradeUser(user) {
  if (user.point > 10) {
    // long upgrade logic...
  }
}

// good
function upgradeUser(user) {
  if (user.point <= 10) {
    return;
  }
  // long upgrade logic...
}

// First-class function
// functions are treated like any other vaiable
// can be assigned as a value to variable
// can be passed as an argument to other functions.
// can be returned by another function

// 1. Function expression
// a function declaration can be called than it is defined.(hoisted)
// a function expression is created when the execution reaches it.

// anonymous function
const print = function () {
  console.log("print");
};
print();
const printAgain = print;
printAgain();
const sumAgain = sum;
console.log(sumAgain(1, 3));

// 2. Callback function using function expression
function randomQuiz(answer, printYes, printNo) {
  if (answer === "love you") {
    printYes();
  } else {
    printNo();
  }
}

// anonymous function
const printYes = function () {
  console.log("yes!");
};
// named function
// better debugging in debugger's stack traces
// revursions
const printNo = function print() {
  console.log("no!");
  //print(); // recursive call
};

randomQuiz("wrong", printYes, printNo);
randomQuiz("love you", printYes, printNo);

// Arrow function
// always anonymous
const simplePrint = function () {
  console.log("simplePrint");
};

const arrowSimplePrint = () => console.log("arrow simplePrint!");
const add = (a, b) => a + b;
// const add = function (a, b) {
//   return a + b;
// };

const simpleMultiply = (a, b) => {
  // do something more
  return a * b;
};

// IIFE: Immediately Invoked Function Expression
(function hello() {
  console.log("IIFE");
})();

// Fun quiz time
// function calcurate(command, a, b)
// command: add, substract, divide, multiply, remainder

function calcurate(command, a, b) {
  switch (command) {
    case "add":
      return a + b;
    //break;
    case "substract":
      return a - b;
    //break;
    case "divide":
      return a / b;
    //break;
    case "multiply":
      return a * b;
    //break;
    case "remainder":
      return a % b;
    //break;
    default:
      throw Error("unknown arithmetic command"); //throw new Error("unknown arithmetic command");
  }
}

console.log(calcurate("add", 5, 3));
console.log(calcurate("substract", 5, 3));
console.log(calcurate("divide", 9, 3));
console.log(calcurate("multiply", 7, 3));
console.log(calcurate("remainder", 13, 3));
